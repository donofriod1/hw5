PGM = keeplog
cc = gcc
C_FLAGS = -c
OBJECTS = $(addsuffix .o, $(SRC)) # all '.o' files
SRC1 = $(wildcard *.c) # all '.c' files
SRC = $(patsubst %.c,%,$(SRC1)) #all '.c' files with '.c' stripped off
all:	$(PGM)
$(PGM):	$(OBJECTS)
		$(cc) -s -o $(PGM) $(OBJECTS) -lkeeplog_helper1 -lkeeplog_helper2 -llist
$(OBJECTS):	%.o: %.c
		$(cc) $(C_FLAGS) $<
		$(cc) -MM $< > $*.d
-include $(addsuffix .d,$(SRC))
clean:
		-rm $(PGM) *.o *.d
